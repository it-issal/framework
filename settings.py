#-*- coding: utf-8 -*-

from .utils import *

DEBUG = True

from socket import gethostname

ALLOWED_HOSTS = [
    gethostname(), # For internal OpenShift load balancer security purposes.
    os.environ.get('OPENSHIFT_APP_DNS'), # Dynamically map to the OpenShift gear name.
]

DEFAULT_APPs = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'django_extensions',
    'neo4django.graph_auth',

    'it_issal.contrib.connect',
)

DEFAULT_MIDDLEWAREs = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'it_issal.middlewares.routing.Augment',
    'it_issal.middlewares.rendering.ThemeSupport',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",

    "it_issal.middlewares.routing.Extend",
    "it_issal.middlewares.rendering.Magnify",
)

MIDDLEWARE_CLASSES = DEFAULT_MIDDLEWAREs

LANGUAGE_CODE = 'fr-FR'
TIME_ZONE = 'UTC'

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

MEDIA_ROOT = FILES_DIR('media')
STATIC_ROOT = FILES_DIR('static')

USE_I18N = True
USE_L10N = True
USE_TZ = True

################################################################################

AUTHENTICATION_BACKENDS = ('neo4django.graph_auth.backends.NodeModelBackend',)

AUTH_USER_MODEL = 'it_issal.contrib.connect.models.Person'

################################################################################

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':   FILES_DIR('main.sqlite3'),
    },
}

NEO4J_DATABASES = {
    'local': {
        'HOST':     'localhost',
        'PORT':     7474,
        'ENDPOINT': '/db/data',
    },
}

MONGO_DATABASES = {
    'local': {
        'HOST': 'localhost',
        'PORT': 27017,
        'NAME': 'it_hole',
    },
}

################################################################################

if 'CLEARDB_DATABASE_URL' in os.environ:
    endpoint = urlparse(os.environ['CLEARDB_DATABASE_URL'])

    DATABASES['clear'] = {
        
    }

#*******************************************************************************

if '' in os.environ:
    endpoint = urlparse(os.environ[''])

    DATABASES['heroku'] = {
        
    }

#*******************************************************************************

if 'GRAPHENEDB_URL' in os.environ:
    endpoint = urlparse(os.environ['GRAPHENEDB_URL'])

    NEO4J_DATABASES['heroku'] = {
        'HOST':'localhost',
        'PORT':7474,
        'ENDPOINT':'/db/data'
    }

#*******************************************************************************

if 'MONGOLAB_URI' in os.environ:
    endpoint = urlparse(os.environ['MONGOLAB_URI'])

    MONGO_DATABASES['heroku'] = {
        'HOST':'localhost',
        'PORT':7474,
        'ENDPOINT':'/db/data'
    }

################################################################################

THEMES = []

for key in [('static',)] + [(x,'themes') for x in ('assets', 'cdn', 'templates')]:
    if not os.path.exists(FILES_DIR(*key)):
        os.system('mkdir -p ' + FILES_DIR(*key))

for key in os.listdir(FILES_DIR('themes')):
    if key not in THEMES:
        for source,target in [
            ('assets', None),
            ('cdn',    None),
            ('views',  'templates'),
        ]:
            pth = FILES_DIR(target or source, 'themes', key)

            if not os.path.exists(pth):
                os.system('ln -sf %s %s' % (FILES_DIR('themes', key, source), pth))

        THEMES += [key]

THEMES = set(THEMES)
