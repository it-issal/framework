#-*- coding: utf-8 -*-

import os, sys, re, operator

from urlparse import urlparse

#*******************************************************************************

import mongoengine as mongo

#*******************************************************************************

from bulbs             import property         as Graph_STD

from bulbs.model       import Node             as Graph_Node
from bulbs.model       import Relationship     as Graph_Edge

from bulbs.neo4jserver import Graph            as Graph_Server
from bulbs.neo4jserver import Config           as Graph_Config

from bulbs.utils       import current_datetime as Graph_Now

#*******************************************************************************

from django.shortcuts import render

#from django.template import TemplateNotFound
TemplateNotFound = Exception
