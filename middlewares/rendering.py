#-*- coding: utf-8 -*-

from it_issal.utils import *

from django.core.urlresolvers import set_urlconf

################################################################################

class BaseMiddleware(object):
    @property
    def config(self):
        if not hasattr(self, '_cfg'):
            import yaml

            self._cfg = yaml.load(open(FILES_DIR('django.yml')).read())

        return self._cfg

################################################################################

class ThemeSupport(BaseMiddleware):
    def process_request(self, request):
        pass

#*******************************************************************************

def Magnify(request):
    #from it_issal.contrib.connect.models import Person

    resp = dict(
        stats = dict(
            people  = 0, #len(Person.objects.all()),
        ),
    )

    #resp['stats']['people'] = reduce(operator.add, [resp['stats'][x] for x in ('pros', 'traders')])

    #for key in ['brand']:
    #    resp[key] = getattr(request, key)

    return resp

