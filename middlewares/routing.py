#-*- coding: utf-8 -*-

from it_issal.utils import *

from django.core.urlresolvers import set_urlconf

################################################################################

class BaseMiddleware(object):
    @property
    def config(self):
        if not hasattr(self, '_cfg'):
            import yaml

            self._cfg = yaml.load(open(FILES_DIR('django.yml')).read())

        return self._cfg

################################################################################

class Augment(BaseMiddleware):
    def process_request(self, request):
        host = request.get_host()

        for entry in self.config['routing']:
            if host in entry['fqdn']:
                try:
                    set_urlconf(entry['urls'])

                    request.urlconf = entry['urls']
                    #return callback(request, **kwargs)
                finally:
                    set_urlconf(None)

                if 'tenant' in entry:
                    setattr(request, 'tenant', entry['tenant'])

    def staled_request(self, request):
        ns = self.config['namespace']

        host = request.get_host()

        if host.startswith(ns+'.'):
            host = 'www.' + host

        sep = '.%s.' % ns

        setattr(request, 'brand', None)

        if sep in host:
            lst = host.split(sep)

            setattr(request, 'dns', {
                'sub': lst[0],
                'qdn': ns,
                'tld': lst[1],
            })

            from webijas.core.models import Alliance

            for brand in Alliance.DEFAULTs:
                if brand['alias']==request.dns['tld']:
                    request.brand,st = Alliance.objects.get_or_create(alias=brand['alias'])

                    for key in brand.keys():
                        if key not in ['alias']:
                            setattr(request.brand, key, brand[key])

                    request.brand.save()

            if request.brand is not None:
                target = request.brand.routing(request)

                if target is not None:
                    request.urlconf = target

                    try:
                        set_urlconf(target)

                        #return callback(request, **kwargs)
                    finally:
                        set_urlconf(None)
        else:
            pass # print host, sep

#*******************************************************************************

def Extend(request):
    resp = {}

    for key in ['tenant']:
        if hasattr(request, key):
            resp[key] = getattr(request, key)

    return resp

