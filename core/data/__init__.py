#-*- coding: utf-8 -*-

from it_issal.helpers import *

#*******************************************************************************

class ORM(object):
    def __init__(self):
        self._grp = {}
        self._doc = {}

    ############################################################################

    def connect_mongo(self, alias, *args, **kwargs):
        if alias not in self._doc:
            self._doc[alias] = mongo.connect(alias, *args, **kwargs)

        return self._doc[alias]

    #***************************************************************************

    def register_schema(self, *args, **kwargs):
        def do_apply(handler, alias, graphs=['default']):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def database(self, key, alias=None, default=None):
        if key in self._doc:
            cnx = self._doc[key]

            return cnx[alias or key]
        else:
            return default

    #***************************************************************************

    def schema(self, ns, key, default=None):
        db = self.database(ns, None, None)

        if db is not None:
            return db[key]

        return default

    ############################################################################

    def connect_neo4j(self, alias, *args, **kwargs):
        if alias not in self._grp:
            self._grp[alias] = Graph_Server(Graph_Config(alias, *args, **kwargs))

        return self._grp[alias]

    #***************************************************************************

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias, graphs=['default']):
            for key in graphs:
                if key in self._grp:
                    self._grp[key].add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_edge(self, *args, **kwargs):
        def do_apply(handler, alias, graphs=['default']):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def graph(self, key, default=None):
        if key in self._grp:
            return self._grp[key]
        else:
            return default
