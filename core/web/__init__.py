#-*- coding: utf-8 -*-

from it_issal.helpers import *

from django.conf import urls as dj_urls

from . import decorators

#*******************************************************************************

class VirtualHost(object):
    def __init__(self, ns, alias, domaines=[], **opts):
        self._mod = ns
        self._key = alias

        self._dns = domaines
        self._cfg = opts

        self._url = []

    namespace = property(lambda self: self._mod)
    alias     = property(lambda self: self._key)

    domaines  = property(lambda self: self._dns)
    config    = property(lambda self: self._cfg)

    def register(self, *args, **kwargs):
        def do_apply(handler, pattern, **opts):
            ptn = dj_urls.url(pattern, handler, **opts)

            self._url.append(ptn)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    @property
    def urlpatterns(self):
        return self._url

