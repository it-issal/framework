#-*- coding: utf-8 -*-

from it_issal.helpers import *

#*******************************************************************************

class RequestMixin(object):
    def __init__(self, request, *args, **kwargs):
        self._req = request

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    request = property(lambda self: self._req)

#*******************************************************************************

class Page(RequestMixin):
    def initialize(self):
        self.title   = ""
        self.summary = ""
        self.tags    = []

        self.layout  = 'homepage'
        self._skin   = 'artificial-reason'

    path     = property(lambda self: self.request.path[1:])
    theme    = property(lambda self: self._skin)

    layout_template = property(lambda self: 'themes/%s/layouts/%s.html' % (
        self.theme,
        self.layout,
    ))
    tld_template = property(lambda self: 'tld.%s/layouts/%s.html' % (
        self.request.tenant['tld'],
        self.layout,
    ))

    @property
    def context(self):
        resp = {}
        
        for target,lst in [
            (self,         ['title','path','theme','layout','']),
            (self.request, ['path']),
        ]:
            for key in lst:
                resp[key] = getattr(target, key)
        
        for key,value in self.request.tenant.iteritems():
            resp['tenant_' + key] = value
        
        return resp

    @property
    def templates(self):
        resp = [
            self.tld_template,
            self.layout_template,
        ]
        
        for key in []:
            pass
        
        return resp

    def render(self, **context):
        resp = None
        
        context.update(**self.context)
        
        for tpl in self.templates:
            try:
                resp = dj_render(self.request, **context)
            except TemplateNotFound,ex:
                pass
        
        return resp

#*******************************************************************************

class DataStore(RequestMixin):
    def initialize(self):
        self._dsn = {}

    request = property(lambda self: self._req)

#*******************************************************************************

class Analytics(RequestMixin):
    def initialize(self):
        self._dsn = {}

    people = property(lambda self: self['']+self[''])

