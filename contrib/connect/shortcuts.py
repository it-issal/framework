#-*- coding: utf-8 -*-

from it_issal.shortcuts import *

################################################################################

from .models import *
from .graph  import *
from .schema import *
from .tasks  import *

################################################################################

vhost = VirtualHost(__module__, 'connect',
    domains = [],
    color   = 'red',
)

