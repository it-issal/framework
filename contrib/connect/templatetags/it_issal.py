from django import template
from django.template.defaultfilters import stringfilter
from django.template.base import add_to_builtins

#*******************************************************************************

add_to_builtins('django.templatetags.static')

register = template.Library()

################################################################################

@register.filter(name='cut')
def cut(value, arg):
    return value.replace(arg, '')

#*******************************************************************************

#@register.filter
#@stringfilter
#def lower(value):
#    return value.lower()

#*******************************************************************************

@register.filter(expects_localtime=True)
def businesshours(value):
    try:
        return 9 <= value.hour < 17
    except AttributeError:
        return ''

################################################################################

register.simple_tag(lambda *x: '/'.join(['themes']+[y in x]), name='theme')

#*******************************************************************************

@register.simple_tag(name='minustwo', takes_context=True)
def some_function(value):
    return value - 2

#*******************************************************************************

# {% current_time "%Y-%m-%d %I:%M %p" as now %}

@register.assignment_tag
def current_time(format_string):
    return datetime.datetime.now().strftime(format_string)

################################################################################

@register.inclusion_tag('link.html', takes_context=True)
def jump_link(context):
    return {
        'link': context['home_link'],
        'title': context['home_title'],
    }

