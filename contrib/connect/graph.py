#-*- coding: utf-8 -*-

from it_issal.shortcuts import *

################################################################################

@ORM.register_node('person')
class Person(Graph_Node):
    element_type = "person"

    name = Graph_STD.String(nullable=False)
    age  = Graph_STD.Integer()

#*******************************************************************************

@ORM.register_edge('person')
class Knows(Graph_Edge):
    label = "knows"

    created = Graph_STD.DateTime(default=Graph_Now, nullable=False)

################################################################################

#class Person(models.NodeModel):
#    name = models.StringProperty()
#    age = models.IntegerProperty()
#
#    friends = models.Relationship('self', rel_type='friends_with')

#class Brand(models.NodeModel):
#    owner      = models.Relationship('Person', rel_type='owns',      related_name='brands', single=True)
#    legal_name = models.StringProperty()
