#-*- coding: utf-8 -*-

from it_issal.contrib.connect.shortcuts import *

################################################################################

@vhost.register(r'^$', name='homepage'),
def homepage(request):
    return render(request, 'connect/homepage.html')

################################################################################

urlpatterns = vhost.urlpatterns
