#-*- coding: utf-8 -*-

from it_issal.shortcuts import *

from neo4django.db import models as Graph
from neo4django.graph_auth.models import User as GraphUser, UserManager

################################################################################

#class Person(models.Model):
#    name = models.CharField(max_length)
#    age = models.IntegerProperty()

#    friends = models.Relationship('self',rel_type='friends_with')

class Person(GraphUser):
    legal_name  = Graph.StringProperty()

    objects = UserManager()
    follows = Graph.Relationship('self', rel_type='follows', related_name='followed_by')

#*******************************************************************************

class Brand(models.NodeModel):
    legal_name  = Graph.StringProperty()

    founders = Graph.Relationship('self', rel_type='follows', related_name='followed_by')

################################################################################

#class Company(Brand):
#    pass

#*******************************************************************************

#class Venture(Brand):
#    pass

################################################################################

class Post(models.NodeModel):
    title  = Graph.StringProperty()
    author = Graph.Relationship(Person, rel_type='written_by', single=True, related_name='posts')

