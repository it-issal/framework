#-*- coding: utf-8 -*-

from it_issal.shortcuts import *

vhost = Host(__module__, 'landing',
    domains = [],
    color   = 'blue',
)

################################################################################

@vhost.register(r'^(.*)\.(htm|html)$', name='dyn_page'),
@vhost.register(r'^$',                 name='homepage'),
def dynamic_page(request, path='homepage'):
    if hasattr(request, 'tenant'):
        request.tenant['path'] = path

        try:
            return render(request, 'pages/%(tld)s/%(sub)s/%(path)s.html' % request.tenant)
        except TemplateNotFound,ex:
            return render(request, 'errors/not-found.html')
    else:
        return render(request, 'errors/not-defined.html')

################################################################################

urlpatterns = vhost.urlpatterns
