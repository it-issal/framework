#-*- coding: utf-8 -*-

from it_issal.shortcuts import *

vhost = Host(__module__, 'landing',
    domains = [],
    color   = 'blue',
)

################################################################################

from django.contrib import admin

admin.autodiscover()

vhost.include(r'^$', dj_urls.include(admin.site.urls))

################################################################################

urlpatterns = vhost.urlpatterns
