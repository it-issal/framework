#-*- coding: utf-8 -*-

from .helpers import *

DJ_PROJECT_DIR = os.path.dirname(__file__)

ROOT_DIR = os.path.dirname(DJ_PROJECT_DIR)

FILES_DIR = lambda *args: os.path.join(ROOT_DIR, 'files', *args)

from .core.data import ORM
from .core.web  import VirtualHost
#from .core.data import 
